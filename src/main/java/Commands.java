import java.util.Scanner;

public class Commands {
    private Parser p;
    private Scanner s;
    private JenaManager m;

    public void test(String source) {
        m = new JenaManager(source);
        s = new Scanner(System.in);
        p = new Parser();
        initCommands();
        requestInput();
    }

    private void initCommands() {
        //Создание класса
        p.registerCommand("c_cl <name>", args -> m.createClass(args[0]));
        //Добавление подкласса
        p.registerCommand("a_sub <super_name> <sub_name>", args -> m.addSubClass(args[0], args[1]));
        //Добавление индивида
        p.registerCommand("a_ind <class_name> <ind_name>", args -> m.addIndividual(args[0], args[1]));
        //Создание свойства
        p.registerCommand("c_pr <pr_name>", args -> m.createDatatypeProperty(args[0]));
        //Добавление свойства
        p.registerCommand("a_pr <ind_name> <pr_name> <value>", args -> m.addProperty(args[0],args[1],args[2]));
        //Изменение свойства
        p.registerCommand("сh_pr <ind_name> <pr_name> <value>", args -> m.changePropertyValue(args[0],args[1],args[2]));
        //Вывод иерархического дерева
        p.registerCommand("list_ht", args -> m.listClassHierarchyTree());
        //Вывод индивидов и их свойств
        p.registerCommand("list_ind", args -> m.listIndividuals());
        //Опрос 'greater than'
        p.registerCommand("q_great <pr_name> <int_value>", args -> m.queryIsGreaterThan(args[0], args[1]));
        //Опрос 'less than'
        p.registerCommand("q_less <pr_name> <int_value>", args -> m.queryIsLessThan(args[0], args[1]));
        //Опрос 'equals'
        p.registerCommand("q_is <pr_name> <int_value>", args -> m.queryIs(args[0], args[1]));
        //Опрос 'contains'/'not contains'
        p.registerCommand("q_is <pr_name> <str_value> <flag>", args -> m.queryContains(args[0], args[1], args[2]));
        //Сохранение в файл
        p.registerCommand("safe <path>", args -> m.safe(args[0]));
        //Выход
        p.registerCommand("exit", args -> System.exit(0));
        //Вывод доступных команд
        p.registerCommand("help", args -> {
            System.out.println("Commands:");
            for(String cmd : p.getRegisteredCommands()) {
                System.out.println(cmd);
            }
        });
        p.registerCommand("help <cmd...>", args -> {
            String[] suggestions = p.suggestions(args[0]);
            if(suggestions.length > 0) {
                System.out.println("Usage:");
                for(String suggestion : suggestions) {
                    System.out.println("  " + suggestion);
                }
            } else {
                System.out.println("Unknown Command!");
            }

        });
    }

    private void requestInput() {
        String input = s.nextLine();
        try {
            p.parse(input);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        requestInput();
    }

}

