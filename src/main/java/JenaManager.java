import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.*;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Iterator;

public class JenaManager {
    OntModel model;
    String source;
    String ns;

    JenaManager(String source) {
        this.source = source;
        this.ns = source + "#";
        model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
    }

    public boolean exists(String name) {
        Resource resourceToSearch = ResourceFactory.createResource(ns + name);
        return model.containsResource(resourceToSearch);
    }

    public void createClass(String name) {
        model.createClass(ns + name);
    }

    public void createDatatypeProperty(String propertyName) {
        model.createDatatypeProperty(ns+ propertyName);
    }

    @Nullable
    private OntClass getClass(String name) {
        return model.getOntClass(name);
    }

    @Nullable
    private Individual getIndividual(String name) { return model.getIndividual(name); }

    @Nullable
    private DatatypeProperty getDatatypeProperty(String name) { return model.getDatatypeProperty(name); }

    public void addSubClass(String className, String subClassName) {
        OntClass superClass = getClass(ns + className);
        //Если исходный класс существует
        if (superClass != null) {
            OntClass subClass = getClass(ns + subClassName);
            //Если подкласс уже имеется или такого подкласса не существует вообще
            if (!superClass.hasSubClass(subClass) && subClass != null) {
                superClass.addSubClass(subClass);
            }
        }
    }

    public void addIndividual(String className, String individualName) {
        OntClass superClass = getClass(ns + className);
        //Если исходный класс существует
        if (superClass != null) {
            //Если индивид с таким именем уже существует - ничего не произойдет
            superClass.createIndividual(ns + individualName);
        }
    }

    public void addProperty(String individualName, String propertyName, String value) {
        Individual individual = getIndividual(ns + individualName);
        DatatypeProperty datatypeProperty = getDatatypeProperty(ns + propertyName);
        //Проверяем существует ли индивид и свойство
        if (individual != null && datatypeProperty != null) {
            individual.addProperty(datatypeProperty, value);
        }
    }

    public void changePropertyValue(String individualName, String propertyName, String value) {
        Individual individual = getIndividual(ns + individualName);
        DatatypeProperty datatypeProperty = getDatatypeProperty(ns + propertyName);
        //Проверяем существует ли индивид и свойство
        if (individual != null && datatypeProperty != null) {
            Literal literal = model.createLiteral(value);
            individual.setPropertyValue(datatypeProperty, literal);
        }
    }

    private void getSubClasses(String name, int level) {
        String tab = "-".repeat(level);
        level++;
        OntClass ontClass = model.getOntClass(ns + name);
        if (ontClass.hasSubClass()) {
            for (Iterator<OntClass> i = ontClass.listSubClasses(); i.hasNext(); ) {
                OntClass c = i.next();
                System.out.println(tab + c.getLocalName());
                getSubClasses(c.getLocalName(), level);
            }
        }
    }

    public void listClassHierarchyTree() {
        for (Iterator<OntClass> i = model.listHierarchyRootClasses(); i.hasNext(); ) {
            OntClass c = i.next();
            System.out.println(c.getLocalName());
            getSubClasses(c.getLocalName(), 1);
        }
    }

    public void listIndividuals() {
        for (Iterator<Individual> i = model.listIndividuals(); i.hasNext(); ) {
            Individual individual = i.next();
            System.out.println(individual.getLocalName());
            for (StmtIterator j = individual.listProperties(); j.hasNext(); ) {
                Statement statement = j.next();
                Property property = statement.getPredicate();
                DatatypeProperty datatypeProperty = model.getDatatypeProperty(property.getURI());
                if (datatypeProperty != null) {
                    System.out.println(" " + statement.getPredicate().getLocalName() + " = " +  statement.getLiteral());
                }
            }
        }
    }

    public void queryIsLessThan(String propertyName, String value) {
        try {
            DatatypeProperty property = getDatatypeProperty(ns + propertyName);
            if (property != null) {
                SimpleSelector selector = new SimpleSelector(null, property, (RDFNode) null) {
                    public boolean selects(Statement s) {
                        return s.getInt() < Integer.parseInt(value);
                    }
                };
                StmtIterator iterator = model.listStatements(selector);
                System.out.println("Individuals with " + property.getLocalName() + " less than " + value);
                while (iterator.hasNext()) {
                    Statement statement = iterator.nextStatement();
                    System.out.println(statement.getSubject().getLocalName());
                }
            }
        }
        catch (Exception e) {

        }
    }

    public void queryIsGreaterThan(String propertyName, String value) {
        try {
            DatatypeProperty property = getDatatypeProperty(ns + propertyName);
            if (property != null) {
                SimpleSelector selector = new SimpleSelector(null, property, (RDFNode) null) {
                    public boolean selects(Statement s) {
                        return s.getInt() > Integer.parseInt(value);
                    }
                };
                StmtIterator iterator = model.listStatements(selector);
                System.out.println("Individuals with " + property.getLocalName() + " more than " + value);
                while (iterator.hasNext()) {
                    Statement statement = iterator.nextStatement();
                    System.out.println(statement.getSubject().getLocalName());
                }
            }
        }
        catch (Exception e) {

        }
    }

    public void queryIs(String propertyName, String value) {
        try {
            DatatypeProperty property = getDatatypeProperty(ns + propertyName);
            if (property != null) {
                SimpleSelector selector = new SimpleSelector(null, property, (RDFNode) null) {
                    public boolean selects(Statement s) {
                        return s.getInt() == Integer.parseInt(value);
                    }
                };
                StmtIterator iterator = model.listStatements(selector);
                System.out.println("Individuals with " + property.getLocalName() + " equals a value " + value);
                while (iterator.hasNext()) {
                    Statement statement = iterator.nextStatement();
                    System.out.println(statement.getSubject().getLocalName());
                }
            }
        }
        catch (Exception e) {

        }
    }

    public void queryContains(String propertyName, String value, String bool) {
        boolean flag = Boolean.parseBoolean(bool);
        try {
            DatatypeProperty property = getDatatypeProperty(ns + propertyName);
            if (property != null) {
                SimpleSelector selector = new SimpleSelector(null, property, (RDFNode) null) {
                    public boolean selects(Statement s) {
                        //flag == true -> contains logic
                        //flag == false -> not contains logic
                        return flag == s.getString().equals(value);
                    }
                };
                StmtIterator iterator = model.listStatements(selector);
                System.out.println("Individuals with " + property.getLocalName() + " more than " + value);
                while (iterator.hasNext()) {
                    Statement statement = iterator.nextStatement();
                    System.out.println(statement.getSubject().getLocalName());
                }
            }
        }
        catch (Exception e) {

        }
    }


    public void safe(String pathName) {
        //Сохранение файла
        model.write(System.out);
        File file = new File(pathName);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            model.write(fileOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}

